package com.jdbc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static Connection con = null;

    static {
        String url = "jdbc:postgresql://localhost:5432/postgres";
//        String user = "root";
//        String pass = "root";
        String user = null;
        String pass = null;
        try {
            Class.forName("org.postgresql.Driver");
            if (user != null && pass != null) {
                con = DriverManager.getConnection(url, user, pass);
            }
            con = DriverManager.getConnection(url);
            System.out.println("### DATABASE CONNECTION SUCCESSFULLY OPENED ###");
        }
        catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection()
    {
        return con;
    }

    public static void close()
    {
        try {
            if (con != null) {
                System.out.println("### DATABASE CONNECTION SUCCESSFULLY CLOSED ###");
                con.close();
                con = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
