package com.jdbc.model;

public class Phone extends Product{
    private float screenSize;
    private String cpu;
    private int memory;
    private int ram;
    private float weight;
    private String os;
    private String colour;
    private boolean touchId;
    private boolean faceId;

    public float getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(float screenSize) {
        this.screenSize = screenSize;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public boolean isTouchId() {
        return touchId;
    }

    public void setTouchId(boolean touchId) {
        this.touchId = touchId;
    }

    public boolean isFaceId() {
        return faceId;
    }

    public void setFaceId(boolean faceId) {
        this.faceId = faceId;
    }

    public Phone() {}

    public Phone(String name, String manufacturer, int count, float screenSize, String cpu, int memory, int ram, float weight, String os, String colour, boolean touchId, boolean faceId) {
        super(name, manufacturer, count);
        this.screenSize = screenSize;
        this.cpu = cpu;
        this.memory = memory;
        this.ram = ram;
        this.weight = weight;
        this.os = os;
        this.colour = colour;
        this.touchId = touchId;
        this.faceId = faceId;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "name=" + super.getName() +
                ", manufacturer=" + super.getManufacturer() +
                ", count=" + super.getCount() +
                ", screenSize=" + screenSize +
                ", cpu='" + cpu + '\'' +
                ", memory=" + memory +
                ", ram=" + ram +
                ", weight=" + weight +
                ", os=" + os +
                ", colour='" + colour + '\'' +
                ", touchId=" + touchId +
                ", faceId=" + faceId +
                '}';
    }
}