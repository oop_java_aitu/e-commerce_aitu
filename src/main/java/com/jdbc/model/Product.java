package com.jdbc.model;

public class Product {
    private int id;
    private String name;
    private String manufacturer;
    private int count;

    public Product() {}

    public Product(String name, String manufacturer, int count) {
        setName(name);
        setManufacturer(manufacturer);
        setCount(count);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", count=" + count +
                '}';
    }
}