package com.jdbc.main;

import java.sql.*;
import java.util.List;

import com.jdbc.util.DatabaseConnection;
import com.jdbc.dao.PhoneDaoImplementation;
import com.jdbc.model.Phone;

public class Application {
    public static void main(String[] args) throws SQLException {

        Connection con = DatabaseConnection.getConnection();
        DatabaseMetaData dbm = con.getMetaData();
        ResultSet tables = dbm.getTables(null, null, "phones", null);
        if (tables.next()) {
            System.out.println("Table already exists.");
        } else {
            String CREATE_TABLE_PHONES_SQL="CREATE TABLE phones ("
                    + "id SERIAL PRIMARY KEY,"
                    + "name VARCHAR(255) NOT NULL,"
                    + "manufacturer VARCHAR(255) NOT NULL,"
                    + "count INTEGER NOT NULL,"
                    + "screen_size FLOAT NOT NULL,"
                    + "cpu VARCHAR(255) NOT NULL,"
                    + "memory INTEGER NOT NULL,"
                    + "ram INTEGER NOT NULL,"
                    + "weight FLOAT NOT NULL,"
                    + "os VARCHAR(255) NOT NULL,"
                    + "colour VARCHAR(255) NOT NULL,"
                    + "touch_id BOOLEAN NOT NULL,"
                    + "face_id BOOLEAN NOT NULL)";

            Statement stmt = con.createStatement();
            stmt.executeUpdate(CREATE_TABLE_PHONES_SQL);
            System.out.println("Created table in given database...");
        }

        Phone phone = new Phone();
        phone.setName("iPhone 12 PRO MAX");
        phone.setManufacturer("APPLE");
        phone.setCount(9);
        phone.setScreenSize((float)5.5);
        phone.setCpu("Apple A13");
        phone.setMemory(256);
        phone.setRam(8);
        phone.setWeight((float)0.2);
        phone.setOs("iOS 13");
        phone.setColour("green");
        phone.setTouchId(true);
        phone.setFaceId(true);

        PhoneDaoImplementation phoneDao = new PhoneDaoImplementation();
        // add
        int id = phoneDao.add(phone);
        phone.setId(id); // set auto-generated id to local phone model

        // read
        Phone phone1 = phoneDao.getPhone(phone.getId());
        System.out.println(phone1.getId() + " " + phone1.getName());

        // read all
        List<Phone> ls = phoneDao.getPhones();
        for (Phone ph : ls) {
            System.out.println(ph);
        }

        // update
        Phone tempPhone = phoneDao.getPhone(phone1.getId());
        tempPhone.setName("Asgard");
        phoneDao.update(tempPhone);
        phone1 = phoneDao.getPhone(phone1.getId()); // again read
        System.out.println("After update: " + phone1.getId() + " " + phone1.getName());


        // delete
        phoneDao.delete(phone.getId());

        DatabaseConnection.close();
    }
}
