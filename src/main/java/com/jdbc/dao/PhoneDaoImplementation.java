package com.jdbc.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


import com.jdbc.model.Phone;
import com.jdbc.util.DatabaseConnection;


public class PhoneDaoImplementation implements PhoneDao {
    static Connection con = DatabaseConnection.getConnection();

    @Override
    public int add(Phone phone) throws SQLException {
        String query
                = "INSERT INTO phones(name, manufacturer, count, screen_size, cpu, memory, ram," +
                " weight, os, colour, touch_id, face_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, phone.getName());
        ps.setString(2, phone.getManufacturer());
        ps.setInt(3, phone.getCount());
        ps.setFloat(4, phone.getScreenSize());
        ps.setString(5, phone.getCpu());
        ps.setInt(6, phone.getMemory());
        ps.setInt(7, phone.getRam());
        ps.setFloat(8, phone.getWeight());
        ps.setString(9, phone.getOs());
        ps.setString(10, phone.getColour());
        ps.setBoolean(11, phone.isTouchId());
        ps.setBoolean(12, phone.isFaceId());

        int n = ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if(rs.next())
        {
            return rs.getInt(1);
        }
        return n;
    }

    @Override
    public void delete(int id) throws SQLException {
        String query
                = "DELETE FROM phones where id = ?";
        PreparedStatement ps
                = con.prepareStatement(query);
        ps.setInt(1, id);
        ps.executeUpdate();
    }

    @Override
    public Phone getPhone(int id)
            throws SQLException
    {

        String query
                = "SELECT * FROM phones where id = ?";
        PreparedStatement ps
                = con.prepareStatement(query);

        ps.setInt(1, id);
        Phone phone = new Phone();
        ResultSet rs = ps.executeQuery();
        boolean check = false;

        while (rs.next()) {
            check = true;
            phone.setId(rs.getInt("id"));
            phone.setName(rs.getString("name"));
            phone.setManufacturer(rs.getString("manufacturer"));
            phone.setCount(rs.getInt("count"));
            phone.setScreenSize(rs.getFloat("screen_size"));
            phone.setCpu(rs.getString("cpu"));
            phone.setMemory(rs.getInt("memory"));
            phone.setRam(rs.getInt("ram"));
            phone.setWeight(rs.getFloat("weight"));
            phone.setOs(rs.getString("os"));
            phone.setColour(rs.getString("colour"));
            phone.setTouchId(rs.getBoolean("touch_id"));
            phone.setFaceId(rs.getBoolean("face_id"));
        }

        if (check) {
            return phone;
        }
        else
            return null;
    }

    @Override
    public List<Phone> getPhones()
            throws SQLException
    {
        String query = "SELECT * FROM phones";
        PreparedStatement ps = con.prepareStatement(query);
        ResultSet rs = ps.executeQuery();
        List<Phone> phones = new ArrayList<>();

        while (rs.next()) {
            Phone phone = new Phone();
            phone.setId(rs.getInt("id"));
            phone.setName(rs.getString("name"));
            phone.setManufacturer(rs.getString("manufacturer"));
            phone.setCount(rs.getInt("count"));
            phone.setScreenSize(rs.getFloat("screen_size"));
            phone.setCpu(rs.getString("cpu"));
            phone.setMemory(rs.getInt("memory"));
            phone.setRam(rs.getInt("ram"));
            phone.setWeight(rs.getFloat("weight"));
            phone.setOs(rs.getString("os"));
            phone.setColour(rs.getString("colour"));
            phone.setTouchId(rs.getBoolean("touch_id"));
            phone.setFaceId(rs.getBoolean("face_id"));
            phones.add(phone);
        }
        return phones;
    }

    @Override
    public void update(Phone phone)
            throws SQLException
    {
        String query
                = "UPDATE phones set name = ?, manufacturer = ?, count = ?, screen_size = ?," +
                " cpu = ?, memory = ?, ram = ?, weight = ?, os = ?, colour = ?, touch_id = ?, face_id = ?" +
                " where id = ?";
        PreparedStatement ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, phone.getName());
        ps.setString(2, phone.getManufacturer());
        ps.setInt(3, phone.getCount());
        ps.setFloat(4, phone.getScreenSize());
        ps.setString(5, phone.getCpu());
        ps.setInt(6, phone.getMemory());
        ps.setInt(7, phone.getRam());
        ps.setFloat(8, phone.getWeight());
        ps.setString(9, phone.getOs());
        ps.setString(10, phone.getColour());
        ps.setBoolean(11, phone.isTouchId());
        ps.setBoolean(12, phone.isFaceId());
        ps.setInt(13, phone.getId());
        ps.executeUpdate();
    }
}