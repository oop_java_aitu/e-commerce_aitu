package com.jdbc.dao;

import java.sql.SQLException;
import java.util.List;

import com.jdbc.model.Phone;

public interface PhoneDao {
    public int add(Phone phone)
            throws SQLException;
    public void delete(int id)
            throws SQLException;
    public Phone getPhone(int id)
            throws SQLException;
    public List<Phone> getPhones()
            throws SQLException;
    public void update(Phone phone)
            throws SQLException;
}